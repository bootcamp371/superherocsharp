﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SuperHeroes.Models;

public partial class SuperDbContext : DbContext
{
    public SuperDbContext()
    {
    }

    public SuperDbContext(DbContextOptions<SuperDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<HeroPower> HeroPowers { get; set; }

    public virtual DbSet<Power> Powers { get; set; }

    public virtual DbSet<SuperHero> SuperHeroes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=SuperDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<HeroPower>(entity =>
        {
            entity.HasKey(e => e.Id); //.HasName("PK__HeroPowe__3214EC073E7E7E1E");

            entity.ToTable("HeroPower");

            //entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<Power>(entity =>
        {
            entity.HasKey(e => e.Id); //.HasName("PK__Powers__3214EC07D213C91D");
            entity.ToTable("Powers");
            //entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.Description)
                .HasMaxLength(100)
                .IsFixedLength();
            entity.Property(e => e.Name)
                .HasMaxLength(30)
                .IsFixedLength();
        });

        modelBuilder.Entity<SuperHero>(entity =>
        {
            entity.HasKey(e => e.Id); //.HasName("PK__SuperHer__3214EC07AF1BC508");

            entity.ToTable("SuperHero");

           // entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.Dob).HasColumnType("date");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .IsFixedLength();
            entity.Property(e => e.NickName)
                .HasMaxLength(20)
                .IsFixedLength();
            entity.Property(e => e.Phone)
                .HasMaxLength(12)
                .IsUnicode(false)
                .IsFixedLength();
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
