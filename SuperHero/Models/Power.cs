﻿using System;
using System.Collections.Generic;

namespace SuperHeroes.Models;

public partial class Power
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }
}
