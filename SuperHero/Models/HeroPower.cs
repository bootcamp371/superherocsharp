﻿using System;
using System.Collections.Generic;

namespace SuperHeroes.Models;

public partial class HeroPower
{
    public int Id { get; set; }

    public int? HeroId { get; set; }

    public int? PowerId { get; set; }

    public string? Name { get; set; }

    public string? Description { get; set; }
}
