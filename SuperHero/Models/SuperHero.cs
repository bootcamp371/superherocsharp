﻿using System;
using System.Collections.Generic;

namespace SuperHeroes.Models;

public partial class SuperHero
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? NickName { get; set; }

    public string? Phone { get; set; }

    public DateTime? Dob { get; set; }

    //public List<HeroPower> HeroPowers { get; set; } = new();
}
