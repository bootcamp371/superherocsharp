﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperHeroes.Models;

namespace SuperHeroes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeroPowerController : ControllerBase
    {
        private readonly SuperDbContext _context;


        public HeroPowerController(SuperDbContext context)
        {
            _context = context;
        }

        // GET: api/HeroPower
        [HttpGet]
        public async Task<ActionResult<IEnumerable<HeroPower>>> GetHeroPowers()
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            return await _context.HeroPowers.ToListAsync();
        }

        // GET: api/HeroPower/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HeroPower>> GetHeroPower(int id)
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            var heroPowerById = await _context.HeroPowers.FindAsync(id);

            if (heroPowerById == null)
            {
                return NotFound();
            }

            return heroPowerById;
        }
        
        // GET: api/HeroPower by heroid
        [HttpGet("{heroId}/heroId")]
        public async Task<ActionResult<IEnumerable<HeroPower>>> GetHeroPowersByHeroId(int heroId)
        {
            if (_context.HeroPowers == null)
            {
                return NotFound();
            }
            var byHero = await _context.HeroPowers.Where(x => x.HeroId == heroId).ToListAsync();
            
            return byHero;
        }

        // GET: api/HeroPower by powerid
        [HttpGet("{powerId}/powerId")]
        public async Task<ActionResult<IEnumerable<SuperHero>>> GetHeroPowersByPowerId(int powerId)
        {
            if (_context.HeroPowers == null)
            {
                return NotFound();
            }
            var heroIdList = await _context.HeroPowers.Where(x => x.PowerId == powerId).Select(x => x.HeroId).ToListAsync();
            var heroes = await _context.SuperHeroes.Where(x => heroIdList.Contains(x.Id)).ToListAsync();
            return heroes;
        }


        // PUT: api/HeroPower/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutHeroPower(int id, HeroPower heroPower)
        {
            if (id != heroPower.Id)
            {
                return BadRequest();
            }

            _context.Entry(heroPower).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HeroPowerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
       
        // POST: api/Animal
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<HeroPower>> PostHeroPowers(HeroPower heroPower)
        {
            if (_context.HeroPowers == null)
            {
                return Problem("Entity set 'HeroDbContext.HeroPower'  is null.");
            }
            _context.HeroPowers.Add(heroPower);
            await _context.SaveChangesAsync();

            return Created("PostHeroPowers", heroPower);
        }
        /*
       // DELETE: api/Animal/5
       [HttpDelete("{id}")]
       public async Task<IActionResult> DeleteAnimal(int id)
       {
           if (_context.Animals == null)
           {
               return NotFound();
           }
           var product = await _context.Animals.FindAsync(id);
           if (product == null)
           {
               return NotFound();
           }


           _context.Animals.Remove(product);
           await _context.SaveChangesAsync();

           return NoContent();
       }
       */
        private bool HeroPowerExists(int id)
        {
            return (_context.HeroPowers?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
