﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperHeroes.Models;

namespace SuperHeroes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperHeroController : ControllerBase
    {
        private readonly SuperDbContext _context;


        public SuperHeroController(SuperDbContext context)
        {
            _context = context;
        }

        // GET: api/SuperHeroes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SuperHero>>> GetSuperHeroes()
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            return await _context.SuperHeroes.ToListAsync();
        }
        
        // GET: api/Animal/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SuperHero>> GetSuperHero(int id)
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            var superHeroById = await _context.SuperHeroes.FindAsync(id);

            if (superHeroById == null)
            {
                return NotFound();
            }

            return superHeroById;
        }
        
        // GET: api/SuperHeros by HeroPowerId
        [HttpGet("{heroId}/heroId")]
        public async Task<ActionResult<IEnumerable<SuperHero>>> GetSuperHerosbyHeroId(int heroId)
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            var byOwner = await _context.SuperHeroes.Where(x => x.Id == heroId).ToListAsync();
            //return await _context.Animals.ToListAsync(owner);
            return byOwner;
        }

        
        // PUT: api/Animal/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperHero(int id, SuperHero superHero)
        {
            if (id != superHero.Id)
            {
                return BadRequest();
            }

            _context.Entry(superHero).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperHeroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        
        // POST: api/SuperHero
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SuperHero>> PostSuperHero(SuperHero superHero)
        {
            if (_context.SuperHeroes == null)
            {
                return Problem("Entity set 'SuperDbContext.SuperHero'  is null.");
            }
            _context.SuperHeroes.Add(superHero);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("PostSuperHero", superHero);
            //return CreatedAtAction("PostSuperHero", new { id = superHero.Id }, superHero);
        }
        
        // DELETE: api/Animal/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperHero(int id)
        {
            if (_context.SuperHeroes == null)
            {
                return NotFound();
            }
            var product = await _context.SuperHeroes.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }


            _context.SuperHeroes.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        
        private bool SuperHeroExists(int id)
        {
            return (_context.SuperHeroes?.Any(e => e.Id == id)).GetValueOrDefault();
        }
        
    }
}
