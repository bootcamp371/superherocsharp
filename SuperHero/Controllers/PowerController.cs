﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperHeroes.Models;

namespace SuperHeroes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PowerController : ControllerBase
    {
        private readonly SuperDbContext _context;


        public PowerController(SuperDbContext context)
        {
            _context = context;
        }

        // GET: api/Power
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Power>>> GetPowers()
        {
            if (_context.Powers == null)
            {
                return NotFound();
            }
            return await _context.Powers.ToListAsync();
        }

        // GET: api/Power/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Power>> GetPowerById(int id)
        {
            if (_context.Powers == null)
            {
                return NotFound();
            }
            var powerById = await _context.Powers.FindAsync(id);

            if (powerById == null)
            {
                return NotFound();
            }

            return powerById;
        }

        // POST: api/Power
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Power>> PostPower(Power power)
        {
            if (_context.Powers == null)
            {
                return Problem("Entity set 'HeroDbContext.Power'  is null.");
            }
            _context.Powers.Add(power);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPowerById", new { id = power.Id }, power);
        }
        /*
        [HttpPost("/convert")]
        public async Task<ActionResult> ConvertIntsToPowers(int[] powerIds)
        {
            if(_context.Powers == null)
            {
                return NotFound();
            }

            List<Power> powers = await _context.Powers.Where(p => powerIds.Contains(p.Id)).ToListAsync();
            if(powers == null)
            {
                return NotFound();
            }

            return Ok(powers);
        }
        */
        // GET: api/Animal
        /* [HttpGet("{owner}/owner")]
         public async Task<ActionResult<IEnumerable<Power>>> GetPowersByHero(string owner)
         {
             if (_context.Powers == null)
             {
                 return NotFound();
             }
             var byOwner = await _context.Powers.Where(x => x.Owner == owner).ToListAsync();
             //return await _context.Animals.ToListAsync(owner);
             return byOwner;
         }*/


        // PUT: api/Power/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPower(int id, Power power)
        {
            if (id != power.Id)
            {
                return BadRequest();
            }

            _context.Entry(power).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PowerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        /*
        // POST: api/Animal
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Animal>> PostAnimal(Animal animal)
        {
            if (_context.Animals == null)
            {
                return Problem("Entity set 'ShelterDbContext.Animal'  is null.");
            }
            _context.Animals.Add(animal);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAnimal", new { id = animal.Id }, animal);
        }

        // DELETE: api/Animal/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAnimal(int id)
        {
            if (_context.Animals == null)
            {
                return NotFound();
            }
            var product = await _context.Animals.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }


            _context.Animals.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        */
        private bool PowerExists(int id)
        {
            return (_context.Powers?.Any(e => e.Id == id)).GetValueOrDefault();
        }


    }
}
