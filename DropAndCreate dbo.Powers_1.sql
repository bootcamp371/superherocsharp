﻿USE [SuperDB]
GO

/****** Object: Table [dbo].[Powers] Script Date: 4/17/2023 4:13:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Powers];


GO
CREATE TABLE [dbo].[Powers] (
    [Id]          INT IDENTITY (1,1) NOT NULL,
    [Name]        NCHAR (30)  NULL,
    [Description] NCHAR (100) NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC)
);


